	<!DOCTYPE html>
<html lang="en">
<?php
require("language/language.php");
require("includes/function.php");
include("includes/head.php");

?>

<script type="text/javascript" src="assets/js/core/app.js"></script>
<script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
<body class="navbar-bottom">

	<!-- Main navbar -->
<?php 
include("includes/header.php");

$qry_cat="SELECT COUNT(*) as num FROM tbl_category";

$total_category= mysqli_fetch_array(mysqli_query($mysqli,$qry_cat));

$total_category = $total_category['num'];


$qry_menu="SELECT COUNT(*) as num FROM tbl_menu_list";

$total_menu= mysqli_fetch_array(mysqli_query($mysqli,$qry_menu));

$total_menu = $total_menu['num'];

$qry_promo="SELECT COUNT(*) as num FROM tbl_promo";

$total_promo= mysqli_fetch_array(mysqli_query($mysqli,$qry_promo));

$total_promo = $total_promo['num'];


$qry_restaurant="SELECT COUNT(*) as num FROM tbl_restaurants";

$total_restaurant = mysqli_fetch_array(mysqli_query($mysqli,$qry_restaurant));

$total_restaurant = $total_restaurant['num'];


$qry_users="SELECT COUNT(*) as num FROM tbl_users";

$total_users = mysqli_fetch_array(mysqli_query($mysqli,$qry_users));

$total_users = $total_users['num'];


$qry_order="SELECT COUNT(*) as num FROM tbl_order_details";

$total_order = mysqli_fetch_array(mysqli_query($mysqli,$qry_order));

$total_order = $total_order['num'];




$userId = $_SESSION['id'];
$is_SuerAdmin = "SELECT is_superadmin FROM tbl_admin WHERE id = ".$userId."";
$superAdmin = mysqli_fetch_array(mysqli_query($mysqli,$is_SuerAdmin));
$isSuperAdmin = false;
if($superAdmin['is_superadmin'] == 1){ 
$isSuperAdmin = true;
}

/******* Total order in current month******** AND status = 'Delivered' */
if($isSuperAdmin ==true) {
/* $month_order = "SELECT *  FROM `tbl_payment`
   WHERE order_date >= LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH
  AND order_date < LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY AND status = 'Delivered'"; */
  
 $month_order ="SELECT *,sum(tod.total_menuorder_price) as TotalOrderAmount FROM `tbl_payment`as tp LEFT JOIN `tbl_order_details` as tod ON tp.order_id = tod.order_unique_id WHERE tp.order_date >= LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH AND tp.order_date < LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY AND tod.order_status = 'Delivered' GROUP BY tod.order_unique_id ";


$query_print = mysqli_query($mysqli,$month_order);
 $totalOrderInMonth = $query_print->num_rows;
 if(isset($totalOrderInMonth) && !empty($totalOrderInMonth)) {
	$totalOrderInMonth = $query_print->num_rows;
} else {
	$totalOrderInMonth = '0';
}
 
/******* Total revenue in current month******** */
/* $monthrevenueQuery = "SELECT SUM(total_price) as total_price
  FROM `tbl_payment`
   WHERE order_date >= LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH
  AND order_date < LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY AND status = 'Delivered'"; */
  
  
  $monthrevenueQuery ="SELECT *,sum(tod.total_menuorder_price) as TotalOrderAmount FROM `tbl_payment`as tp LEFT JOIN `tbl_order_details` as tod ON tp.order_id = tod.order_unique_id WHERE tp.order_date >= LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH AND tp.order_date < LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY AND tod.order_status = 'Delivered' ";
  
$total_revenue = mysqli_fetch_array(mysqli_query($mysqli,$monthrevenueQuery));
if(isset($total_revenue['TotalOrderAmount']) && !empty($total_revenue['TotalOrderAmount'])) {
	$totalMonthRevenue = $total_revenue['TotalOrderAmount'];
} else {
	$totalMonthRevenue = '0';
}



/******* Total order in a day******** */
/* $todayOrderQuery = "SELECT * FROM `tbl_payment` WHERE DATE(order_date) = DATE(NOW()) AND  status = 'Delivered'"; */

$todayOrderQuery = "SELECT *  FROM `tbl_payment`as tp LEFT JOIN `tbl_order_details` as tod ON tp.order_id = tod.order_unique_id WHERE DATE(tp.order_date) = DATE(NOW()) AND tod.order_status = 'Delivered' GROUP BY tod.order_unique_id";


$query_fatch = mysqli_query($mysqli,$todayOrderQuery);
 $totalOrderToday = $query_fatch->num_rows;
if(isset($totalOrderToday) && !empty($totalOrderToday)) {
	$totalOrderToday = $totalOrderToday;
} else {
	$totalOrderToday = '0';
}


/******* Total revenue today******** */
/* $todayrevenuQuery = "SELECT SUM(total_price) as total_price FROM `tbl_payment` WHERE DATE(order_date) = DATE(NOW()) AND  status = 'Delivered'"; */

$todayrevenuQuery = "SELECT sum(tod.total_menuorder_price) as TotalOrderAmount FROM `tbl_payment`as tp LEFT JOIN `tbl_order_details` as tod ON tp.order_id = tod.order_unique_id WHERE DATE(tp.order_date) = DATE(NOW()) AND tod.order_status = 'Delivered'";

$total_revenue_today = mysqli_fetch_array(mysqli_query($mysqli,$todayrevenuQuery));
if(isset($total_revenue_today['total_price']) && !empty($total_revenue_today['total_price'])) {
	$totalTodayRevenue = $total_revenue_today['total_price'];
} else {
	$totalTodayRevenue = '0';
}

 
}




?>
	<!-- /main navbar -->


	<!-- Page header -->
<div class="page-header">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="home.php"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>

	<div class="page-header-content">
		<div class="page-title">
			<h4><span class="text-semibold">Home</span></h4>
		</div>

	</div>
</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include("includes/sidebar.php");?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Dashboard content -->
				<div class="row">
					<div class="col-lg-12">

						<!-- Quick stats boxes -->
						<div class="row">
						<?php if($isSuperAdmin ==true) { ?>
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-teal-400">
								<a href="manage_category.php" class="text-muted text-size-small">
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $totalOrderInMonth;?></span>
										</div>

										<h3 class="no-margin">Total order in month</h3>
										One month
										<div class="text-muted text-size-small">Current month order</div>
										
										
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-teal-400">
								
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $totalOrderToday;?></span>
										</div>

										<h3 class="no-margin">Total order today</h3>
										Today
										<div class="text-muted text-size-small">Today order</div>
										
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-green-400">
								
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $totalTodayRevenue;?></span>
										</div>

										<h3 class="no-margin">Total revenue today</h3>
										Today Revenue
										<div class="text-muted text-size-small">Today total revenue</div>
										
										
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
						
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-pink-400">
								
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $totalMonthRevenue;?></span>
										</div>

										<h3 class="no-margin">Monthly revenue </h3>
										Monthly Revenue
										<div class="text-muted text-size-small">Monthly total revenue</div>
										
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
						
						<?php  } ?>
						
						
						
						
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-teal-400">
								<a href="manage_category.php" class="text-muted text-size-small">
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $total_category;?></span>
										</div>

										<h3 class="no-margin">Category</h3>
										Category List
										<div class="text-muted text-size-small">All Category</div>
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
							
							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-green-400">
								<a href="manage_submenu_list.php" class="text-muted text-size-small">
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $total_menu;?></span>
										</div>

										<h3 class="no-margin">Menu List</h3>
										menu List
										<div class="text-muted text-size-small"> All Menu List</div>
										
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</a>
								</div>
								<!-- /members online -->

							</div>

							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-pink-400">
								<a href="manage_order_list.php" class="text-muted text-size-small">
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $total_order;?></span>
										</div>

										<h3 class="no-margin">Orders</h3>
										Orders List
										<div class="text-muted text-size-small">All Orders List</div>
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>

							<div class="col-lg-4">

								<!-- Members online -->
								<div class="panel bg-blue-400">
								<a href="manage_users.php" class="text-muted text-size-small">
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $total_users;?></span>
										</div>

										<h3 class="no-margin">Users</h3>
										Users List
										<div class="text-muted text-size-small">All Users List</div>
									</div>

									<div class="container-fluid">
										<div id="members-online"></div>
									</div>
								</div>
								<!-- /members online -->

							</div>
							
							<div class="col-lg-4">

								<!-- Current server load -->
								<div class="panel bg-purple-400">
								<a href="manage_promo_code_list.php" class="text-muted text-size-small">
									<div class="panel-body">
										<div class="heading-elements">
											<span class="heading-text badge bg-teal-800"><?php echo $total_promo;?></span>
										</div>
										<h3 class="no-margin">Promo code</h3>
										Promo code List
										<div class="text-muted text-size-small">All Promo code List</div>
									</div>
								</a>

								</div>
								<!-- /current server load -->

							</div>
						</div>
						</div>
						<!-- /quick stats boxes -->
					</div>

				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	<?php include("includes/footer.php");?>
	<!-- /footer -->

</body>
</html>
